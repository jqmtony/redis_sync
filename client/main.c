#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "msg.h"

#define MSG_QUEUE_PATH "/tmp/redis_sync_queue_shopping"

int main(int argc, char** argv)
{
    printf("Hello, I am Redis Replication Client!\n");
    
    key_t  key;
    int msgid;
    
    //Construct system msg queue
    key = ftok(MSG_QUEUE_PATH, 78);
    if(key == -1)
    {
        printf("ftok error : %m\n"); 
        exit(-1);
    }
    msgid= msgget(key, IPC_CREAT | 0666);
    if(msgid== -1)
    {
        printf("msgget error : %m\n");
        exit(-1);
    }
    
    //begin to send msg
    struct  msgbuf msg;
    while(1)
    {
        printf("Input>>");
        bzero(&msg,sizeof(msg));
        gets(msg.mtext);
        msg.mtype = 1;
        printf("\n");
        //printf("Operation>>");
        //scanf("%ld", &msg.mtype);
        msgsnd(msgid , &msg, MSG_MAX_LEN, IPC_NOWAIT);
        switch(errno)
        {
            case EIDRM:
                printf("msg queue is removed.\n");
                exit(0);
            case EINTR:
                printf("msg queue is interepted.\n");
                break;
            case EAGAIN:
                printf("msg queue is full.\n");
                break;
            default:
                printf("Send success!\n");
        }
    }
    
    return 0;
}


